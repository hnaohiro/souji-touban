require 'bundler'
Bundler.require
require_relative 'models'
require_relative 'app'
run Sinatra::Application
