class User < ActiveRecord::Base
  has_many :turns
  has_many :tasks, through: :turns

  validates :name, presence: { message: "can't be blank!" }
  validates :name, uniqueness: { message: 'has already been taken!' }

  scope :with_tasks, -> { all.map { |e| { user: e, tasks: e.tasks } } }
end

class Turn < ActiveRecord::Base
  belongs_to :user
  belongs_to :task

  def self.allocate
    delete_all
    users = User.pluck(:id).shuffle

    Task.pluck(:id).each.with_index do |task, i|
      user = users[i % users.count]
      create(user_id: user, task_id: task)
    end
  end

  def self.slide
    users = User.all
    users << User.first unless User.first.tasks.empty?

    users.each_cons(2) do |user1, user2|
      task = user2.tasks.first
      if task
        delete_all(user_id: user2.id, task_id: task.id)
        create(user_id: user1.id, task_id: task.id)
      end
    end
  end
end

class Task < ActiveRecord::Base
  has_many :turns
  has_many :users, through: :turns

  validates :name, presence: { message: "can't be blank!" }
  validates :name, uniqueness: { message: 'has already been taken!' }
end
