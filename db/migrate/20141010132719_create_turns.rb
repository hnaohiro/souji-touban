class CreateTurns < ActiveRecord::Migration
  def change
    create_table :turns, id: false do |t|
      t.references :user, index: true
      t.references :task, index: true
    end
  end
end
