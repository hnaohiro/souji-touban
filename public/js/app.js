$(function() {
  $('#user-add').submit(function(event) {
    event.preventDefault();

    $.ajax({
      url: '/api/users',
      type: 'POST',
      data: $(this).serialize(),
      dataType: 'json',
      success: function(result, textStatus, xhr) {
        if (result.errors) {
          console.log(result.errors.name);
        } else {
          window.location = '/admin/users'
        }
      },
      error: function(result, textStatus, xhr) {
        console.log(result);
      }
    });
  });

  $('#task-add').submit(function(event) {
    event.preventDefault();

    $.ajax({
      url: '/api/tasks',
      type: 'POST',
      data: $(this).serialize(),
      dataType: 'json',
      success: function(result, textStatus, xhr) {
        if (result.errors) {
          console.log(result.errors.name);
        } else {
          window.location = '/admin/tasks'
        }
      },
      error: function(result, textStatus, xhr) {
        console.log(result);
      }
    });
  });

  $('.user-delete').click(function(event) {
    var id = $(this).attr('value');

    $.ajax({
      url: '/api/users/' + id,
      type: 'DELETE',
      data: { id: id },
      dataType: 'json',
      success: function(result, textStatus, xhr) {
        if (result.errors) {
          console.log(result.errors.name);
        } else {
          window.location = '/admin/users'
        }
      },
      error: function(result, textStatus, xhr) {
        console.log(result);
      }
    });
  });

  $('.task-delete').click(function(event) {
    var id = $(this).attr('value');

    $.ajax({
      url: '/api/tasks/' + id,
      type: 'DELETE',
      data: { id: id },
      dataType: 'json',
      success: function(result, textStatus, xhr) {
        if (result.errors) {
          console.log(result.errors.name);
        } else {
          window.location = '/admin/tasks'
        }
      },
      error: function(result, textStatus, xhr) {
        console.log(result);
      }
    });
  });
});
