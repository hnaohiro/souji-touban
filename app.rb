require 'sinatra/reloader'

get '/' do
  @users = User.all
  erb :index
end

helpers do
  def protected!
    unless authorized?
      response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
      throw(:halt, [401, "Not authorized\n"])
    end
  end

  def authorized?
    @auth ||= Rack::Auth::Basic::Request.new(request.env)
    credentials = [ENV['BASIC_AUTH_USERNAME'], ENV['BASIC_AUTH_PASSWORD']]
    @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == credentials
  end
end

get '/admin/users' do
  protected!
  @users = User.all
  erb :'admin/users/index', layout: :'admin/layout'
end

get '/admin/users/add' do
  protected!
  erb :'admin/users/add', layout: :'admin/layout'
end

get '/admin/tasks' do
  protected!
  @tasks = Task.all
  erb :'admin/tasks/index', layout: :'admin/layout'
end

get '/admin/tasks/add' do
  protected!
  erb :'admin/tasks/add', layout: :'admin/layout'
end

##########
# API
##########

get '/api/users' do
  json User.with_tasks
end

get '/api/users/:id' do
  json User.find(params[:id])
end

post '/api/users' do
  user = User.new params[:user]
  if user.save
    Turn.allocate
    json status: :created
  else
    json errors: user.errors, status: :unprocessable_entity
  end
end

put '/api/users/:id' do
  user = User.find params[:id]
  if user.update params[:user]
    json status: :ok
  else
    json errors: user.errors, status: :unprocessable_entity
  end
end

delete '/api/users/:id' do
  User.find(params[:id]).destroy
  Turn.allocate
  json status: :deleted
end

get '/api/tasks' do
  json Task.all
end

get '/api/tasks/:id' do
  json Task.find(params[:id])
end

post '/api/tasks' do
  task = Task.new params[:task]
  if task.save
    Turn.allocate
    json status: :created
  else
    json errors: task.errors, status: :unprocessable_entity
  end
end

put '/api/tasks/:id' do
  task = Task.find params[:id]
  if task.update params[:task]
    json status: :ok
  else
    json errors: task.errors, status: :unprocessable_entity
  end
end

delete '/api/tasks/:id' do
  Task.find(params[:id]).destroy
  Turn.allocate
  json status: :deleted
end

not_found do
  json status: :not_found
end

error do
  json status: :error
end
